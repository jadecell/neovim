local null_ls_status_ok, null_ls = pcall( require, "null-ls" )
if not null_ls_status_ok then return end

-- https://github.com/jose-elias-alvarez/null-ls.nvim/tree/main/lua/null-ls/builtins/formatting
local formatting = null_ls.builtins.formatting
-- https://github.com/jose-elias-alvarez/null-ls.nvim/tree/main/lua/null-ls/builtins/diagnostics
local diagnostics = null_ls.builtins.diagnostics

null_ls.setup( {
    debug = false,
    sources = {
        -- formatting
        formatting.prettier.with( {
            filetypes = {
                "typescript", "typescriptreact", "html", "css", "javascript"
            },
            extra_args = { "--no-semi", "--single-quote", "--jsx-single-quote" }
        } ), formatting.black.with( { extra_args = { "--fast" } } ),
        formatting.lua_format.with {
            extra_args = {
                "--indent-width=4", "--no-use-tab", "--align-args",
                "--align-parameter", "--single-quote-to-double-quote",
                "--spaces-inside-functiondef-parens",
                "--spaces-inside-functioncall-parens",
                "--spaces-inside-table-braces",
                "--spaces-around-equals-in-field"
            }
        },
        formatting.shfmt
            .with( { extra_args = { "-i", "4", "-bn", "-ci", "-sr" } } ),

        diagnostics.shellcheck
    }
} )
