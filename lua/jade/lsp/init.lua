local status_ok, _ = pcall(require, "lspconfig")
if not status_ok then
	return
end

require("jade.lsp.lsp-installer")
require("jade.lsp.handlers").setup()
