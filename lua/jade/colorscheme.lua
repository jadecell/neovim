local default_colors = require( "kanagawa.colors" ).setup()

-- this will affect all the hl-groups where the redefined colors are used
local my_colors = { sumiInk1 = "#16161d" }

require"kanagawa".setup( { colors = my_colors } )
vim.cmd( "colorscheme kanagawa" )
