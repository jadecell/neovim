local cmp_status_ok, cmp = pcall( require, "cmp" )
if not cmp_status_ok then return end

local snip_status_ok, luasnip = pcall( require, "luasnip" )
if not snip_status_ok then return end

require( "luasnip/loaders/from_vscode" ).lazy_load()

local check_backspace = function()
    local col = vim.fn.col "." - 1
    return col == 0 or vim.fn.getline( "." ):sub( col, col ):match "%s"
end

--   פּ ﯟ   some other good icons
local kind_icons = {
    Text = "",
    Method = "m",
    Function = "",
    Constructor = "",
    Field = "",
    Variable = "",
    Class = "",
    Interface = "",
    Module = "",
    Property = "",
    Unit = "",
    Value = "",
    Enum = "",
    Keyword = "",
    Snippet = "",
    Color = "",
    File = "",
    Reference = "",
    Folder = "",
    EnumMember = "",
    Constant = "",
    Struct = "",
    Event = "",
    Operator = "",
    TypeParameter = ""
}
-- find more here: https://www.nerdfonts.com/cheat-sheet

cmp.setup {
    snippet = {
        expand = function( args )
            luasnip.lsp_expand( args.body ) -- For `luasnip` users.
        end
    },
    window = {
        completion = {
            border = { "╭", "─", "╮", "│", "╯", "─", "╰", "│" },
            scrollbar = "║",
            autocomplete = {
                require( "cmp.types" ).cmp.TriggerEvent.InsertEnter,
                require( "cmp.types" ).cmp.TriggerEvent.TextChanged
            }
        },
        documentation = {
            border = { "╭", "─", "╮", "│", "╯", "─", "╰", "│" },
            winhighlight = "NormalFloat:NormalFloat,FloatBorder:FloatBorder",
            scrollbar = "║"
        }
    },
    mapping = {
        ["<C-k>"] = cmp.mapping.select_prev_item(),
        ["<C-j>"] = cmp.mapping.select_next_item(),
        ["<C-b>"] = cmp.mapping( cmp.mapping.scroll_docs( -1 ), { "i", "c" } ),
        ["<C-f>"] = cmp.mapping( cmp.mapping.scroll_docs( 1 ), { "i", "c" } ),
        ["<C-Space>"] = cmp.mapping( cmp.mapping.complete(), { "i", "c" } ),
        ["<C-y>"] = cmp.config.disable, -- Specify `cmp.config.disable` if you want to remove the default `<C-y>` mapping.
        ["<C-e>"] = cmp.mapping {
            i = cmp.mapping.abort(),
            c = cmp.mapping.close()
        },
        -- Accept currently selected item. If none selected, `select` first item.
        -- Set `select` to `false` to only confirm explicitly selected items.
        ["<CR>"] = cmp.mapping.confirm { select = true },
        ["<Tab>"] = cmp.mapping( function( fallback )
            if cmp.visible() then
                cmp.select_next_item()
            elseif luasnip.expandable() then
                luasnip.expand()
            elseif luasnip.expand_or_jumpable() then
                luasnip.expand_or_jump()
            elseif check_backspace() then
                fallback()
            else
                fallback()
            end
        end, { "i", "s" } ),
        ["<S-Tab>"] = cmp.mapping( function( fallback )
            if cmp.visible() then
                cmp.select_prev_item()
            elseif luasnip.jumpable( -1 ) then
                luasnip.jump( -1 )
            else
                fallback()
            end
        end, { "i", "s" } )
    },
    formatting = {
        fields = { "kind", "abbr", "menu" },
        format = function( entry, vim_item )
            -- Kind icons
            vim_item.kind = string.format( "%s", kind_icons[vim_item.kind] )
            -- vim_item.kind = string.format('%s %s', kind_icons[vim_item.kind], vim_item.kind) -- This concatonates the icons with the name of the item kind
            vim_item.menu = ({
                nvim_lsp = "[LSP]",
                nvim_lua = "[NVIM_LUA]",
                luasnip = "[Snippet]",
                buffer = "[Buffer]",
                path = "[Path]"
            })[entry.source.name]
            return vim_item
        end
    },
    sources = {
        { name = "nvim_lsp" }, { name = "nvim_lua" }, { name = "luasnip" },
        { name = "buffer" }, { name = "path" }
    },
    confirm_opts = { behavior = cmp.ConfirmBehavior.Replace, select = false },
    documentation = {
        border = { "╭", "─", "╮", "│", "╯", "─", "╰", "│" }
    },
    experimental = { ghost_text = true, native_menu = false }
}

-- set max height of items
vim.cmd( [[ set pumheight=6 ]] )
-- set highlights
local highlights = {
    CmpItemKindText = { fg = "#dcd7ba" },
    CmpItemKindFunction = { fg = "#c34043" },
    CmpItemKindClass = { fg = "#ff9e3b" },
    CmpItemKindKeyword = { fg = "#d27e99" },
    CmpItemKindSnippet = { fg = "#1f1f28" },
    CmpItemKindConstructor = { fg = "#957fb8" },
    CmpItemKindVariable = { fg = "#6a9589", bg = "NONE" },
    CmpItemKindInterface = { fg = "#d27e99", bg = "NONE" },
    CmpItemKindFolder = { fg = "#7e9cd8" },
    CmpItemKindReference = { fg = "#7fb4ca" },
    CmpItemKindMethod = { fg = "#c34043" },
    CmpItemMenu = { fg = "#c34043", bg = "#1f1f28" },
    CmpItemAbbr = { fg = "#1f1f28", bg = "NONE" },
    CmpItemAbbrMatch = { fg = "#7e9cd8", bg = "NONE" },
    CmpItemAbbrMatchFuzzy = { fg = "#7fb4ca", bg = "NONE" }
}
vim.api.nvim_set_hl( 0, "CmpBorderedWindow_FloatBorder", { fg = "#1f1f28" } )
for group, hl in pairs( highlights ) do vim.api.nvim_set_hl( 0, group, hl ) end
