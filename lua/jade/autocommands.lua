vim.cmd( [[
    " Reload config on update
    augroup ConfigUpdate
        autocmd!
        autocmd BufWritePost ~/.config/nvim/init.lua source <afile>
        autocmd BufWritePost ~/.config/nvim/lua/*.lua source <afile>
    augroup end

    " Auto format on save
    autocmd BufWritePre * silent! lua vim.lsp.buf.formatting_sync()

    " Turn colorizer on in all buffers
    autocmd BufEnter * silent! ColorizerAttachToBuffer
]] )
